package com.thinkbridge.automation.runner;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(CustomCucumberWithSerenity.class)
@CucumberOptions(plugin = {"pretty"}, features = {"src/test/resources/feature/signUp.feature"}, glue = {
        "com.thinkbridge.automation.stepdef"})

public class SignUpRunner {

}
