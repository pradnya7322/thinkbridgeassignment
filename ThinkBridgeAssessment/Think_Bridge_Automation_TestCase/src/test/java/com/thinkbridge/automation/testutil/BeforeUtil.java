package com.thinkbridge.automation.testutil;
import net.thucydides.core.webdriver.DriverSource;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BeforeUtil implements DriverSource {
	public WebDriver driver;
	String browser;
	DesiredCapabilities capability = new DesiredCapabilities();

	@Override
	public WebDriver newDriver() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("serenity.properties");
			prop.load(input);
		} catch (FileNotFoundException e) {

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		browser = prop.getProperty("browser");
		System.setProperty("webdriver.chrome.driver", "src/test/resources/driver/chromedriver.exe");
		return new ChromeDriver();
	}

	@Override
	public boolean takesScreenshots() {
		// TODO Auto-generated method stub
		return false;
	}
}
