#Author: pradnyasalve9@gmail.com
#Feature: sign up in jaba talks.

Feature: Sign up in jaba talks.
  Scenario: User validates selected language
    Given User launch chrome browser and opens URL in window
    And User is on signup page of jaba talks
    Then User validates selected language

  Scenario Outline: User signing up in jaba talks by entering input fileds
    Given User is on signup page of jaba talks
    When User enters full name <name>
    And User enters organization name <organisationName>
    And User enters email <emailId>
    And User checks checkbox to agree terms and conditon
    And User clicks on Get started button
    Then User validates success message 

    Examples: 
      | name                | organisationName                   | emailId                      |
      | Pradnya Daulat Salve| SRKay Consulting Group	           | pradnyasalve9@gmail.com    |
  