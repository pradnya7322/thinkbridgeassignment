package com.thinkbridge.automation.stepdef;

import static org.junit.Assert.assertEquals;

import com.thinkbridge.automation.ui.BusinessLogicSignUp;

import com.thinkbridge.automation.ui.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.pages.PageObject;

	
public class SignUpStepDef extends PageObject  {
	BusinessLogicSignUp businessLogicSignUp;

	@Given("^User launch chrome browser and opens URL in window$")
	public void openURL() {
		businessLogicSignUp.openUrl();
	}

	@And("^User is on signup page of jaba talks$")
	public void verifyPageTitle() {
		String pageTitle = businessLogicSignUp.getPageTitle();
		assertEquals("Jabatalks", pageTitle);
	}

	@Then("^User validates selected language$")
	public void validateLanguage() {
		String selectedLang = businessLogicSignUp.validateSelectedLang();
		assertEquals("English", selectedLang);
	}

	@When("^User enters full name (.*)$")
	public void enterName(String name) {
		businessLogicSignUp.enterName(name);
	}

	@And("^User enters organization name (.*)$")
	public void enterOrgName(String orgName) {
		businessLogicSignUp.enterOrgName(orgName);
	}

	@And("^User enters email (.*)$")
	public void enterEmail(String email) {
		businessLogicSignUp.enterEmailId(email);
	}
	@And("^User checks checkbox to agree terms and conditon$")
	public void acceptTermCondition()
	{
		businessLogicSignUp.acceptTermCondition();
	}
	@And("^User clicks on Get started button$")
	public void starButtonClick()
	{
		businessLogicSignUp.startButtonClick();
	}
	@Then("^User validates success message$")
	public void validatSuccessMessage()
	{
		String validationMessage=businessLogicSignUp.validateSuccessMessage();
		assertEquals("A welcome email has been sent. Please check your email.", validationMessage);
	}

}

