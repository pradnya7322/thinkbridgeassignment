package com.thinkbridge.automation.locators;

public class SignUpLocators {
	public static final String LANGUAGE_SELECTION_DROPDOWN="//div[@placeholder='Choose Language']";
	public static final String FULL_NAME_TEXTBOX="name";
	public static final String ORG_NAME_TEXTBOX="orgName";
	public static final String EMAIL_TEXTBOX="singUpEmail";
	public static final String  TERM_CHECKBOX="//span[contains(text(),'I agree')]";
	public static final String START_BUTTON="//button[contains(text(),'Get Started')]";
	public static final String 	SUCCESS_ALERT="//div[contains(@class,'alert')]";
		
}
